Some of the most important data in the enterprise can only come into Splunk as syslog. 
Unfortunately, Splunk isn't the best tool for directly receiving syslog data.  
Here, we show you how to configure syslog-ng to collect, organize, and
prepare your data to be easily ingested by a Splunk forwarder.

Additional information and guidance can be found in this project's
[Wiki](https://gitlab.com/rationalcyber/syslog-ng-configuration/wikis/home).

We hope this helps the community and that you find it useful.  If you have suggested 
improvements, please let us know!